import { LightningElement, track } from 'lwc';

export default class SuggestPassword extends LightningElement {

    @track suggestedPassword = null;    // variable to store password
    @track showSuggestion = false;      // variable to show/hide the password suggestion box
    @track inputData = new Object();    // variable to store input
    @track passwordLength = 10;         // variable to generate the password of length equal to this variable's value

    /*
     * If there is nothing in the password box, then this method opens up 
     * dropdown with the generated password suggestion below the password input field.
    */
    suggestRandomPassword(event) {
        if (this.inputData[event.target.name]) {
            this.showSuggestion = false;
            return;
        }
        this.getPasswordSuggestion();
        this.showSuggestion = true;
    }

    //stores the input data
    handleInputChange(event) {
        this.showSuggestion = false;
        this.inputData[event.target.name] = event.target.value;
    }

    //changes the password length value
    handlePasswordLengthChange(event) {
        this.passwordLength = event.target.value;
    }

    // creates a random password from the "chars" string declared at the end of the file.
    getPasswordSuggestion() {
        let password = '';
        for (var i = 0; i < this.passwordLength; i++) {
            var randomNumber = Math.floor(Math.random() * chars.length);
            password += chars.substring(randomNumber, randomNumber + 1);
        }
        this.suggestedPassword = password;
    }

    // selects and fills the password filed with the suggested password
    selectSuggestedPassword() {
        this.showSuggestion = false;
        this.inputData['password'] = this.suggestedPassword;
        this.template.querySelectorAll('lightning-input').forEach(ele => {
            if (ele.name === 'password') {
                ele.value = this.suggestedPassword;
                return;
            }
        });
    }

    //shows the username and password on the console
    showInfo() {
        console.log('username: ', this.inputData['username']);
        console.log('password: ', this.inputData['password']);
    }
}

// A string to generate password from.
const chars = "0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ";